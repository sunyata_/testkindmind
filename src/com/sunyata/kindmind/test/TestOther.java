package com.sunyata.kindmind.test;

import java.util.ArrayList;


import android.database.Cursor;
import android.test.ActivityInstrumentationTestCase2;

import com.robotium.solo.Solo;
import com.sunyata.kindmind.R;
import com.sunyata.kindmind.Database.ContentProviderM;
import com.sunyata.kindmind.List.SortingAlgorithmServiceM;
import com.sunyata.kindmind.main.MainActivityC;
import com.sunyata.kindmind.test.TUtils.BackNavType;
import com.sunyata.kindmind.test.TUtils.LeftOrRight;
//-importing the R file from the project under test
import com.sunyata.kindmind.util.OtherU;

public class TestOther extends ActivityInstrumentationTestCase2<MainActivityC> {
	private Solo mRobotium;
	public TestOther() {
		super(MainActivityC.class);
	}
	protected void setUp() throws Exception { //Is run before each test
		super.setUp();
		mRobotium = TUtils.setup(getInstrumentation(), getActivity());
	}
	protected void tearDown() throws Exception { //Is run after each test
		//super.tearDown(); //This line has been removed in the Robotium example
		TUtils.tearDown(getInstrumentation(), mRobotium, getActivity());
	}

	
	public void testAddItems(){
		TUtils.addListItems(mRobotium, 2, BackNavType.TEMPORAL, "New item nr ");
		TUtils.addListItems(mRobotium, 2, BackNavType.ANCESTRAL, "New item nr ");
	}
	
	public void testOrientationChange(){
		TUtils.switchOrientation(mRobotium, mRobotium.getCurrentActivity());
		TUtils.switchOrientation(mRobotium, mRobotium.getCurrentActivity());
		TUtils.switchOrientation(mRobotium, mRobotium.getCurrentActivity());

		/*
		mRobotium.setActivityOrientation(Solo.LANDSCAPE);
		mRobotium.setActivityOrientation(Solo.PORTRAIT);
		mRobotium.setActivityOrientation(Solo.LANDSCAPE);
		*/
	}
	
	public void testCheckBoxClickAndSave(){
		//Clicking on one button
		mRobotium.clickOnCheckBox(0);
		mRobotium.waitForView(R.id.list_item_activeCheckBox);
		assertTrue(mRobotium.isCheckBoxChecked(0));
		
		//Saving
		mRobotium.clickOnActionBarItem(R.id.menu_item_save_pattern);
		mRobotium.waitForView(R.id.list_item_activeCheckBox);
		assertFalse(mRobotium.isCheckBoxChecked(0));
	}
	
	public void testAlgorithmSinglePattern(){
		
		final String feeling_item_prefix = "Feeling item nr ";
		final String need_item_prefix = "Need item nr ";
		
		//Create three new Feeling items
		TUtils.addListItems(mRobotium, 3, BackNavType.TEMPORAL, feeling_item_prefix);
		
		//Click the new feeling items
		TUtils.clickOnListItemText(mRobotium, feeling_item_prefix + 0);
		TUtils.clickOnListItemText(mRobotium, feeling_item_prefix + 1);
		TUtils.clickOnListItemText(mRobotium, feeling_item_prefix + 2);
		
		//Swipe to the right
		TUtils.scrollSideways(mRobotium, LeftOrRight.RIGHT);
		
		//Adding a new need item
		TUtils.addListItems(mRobotium, 1, BackNavType.TEMPORAL, need_item_prefix);
		
		//Clicking on the new need item
		TUtils.clickOnListItemText(mRobotium, need_item_prefix + 0);
		
		//Saving the new pattern
		TUtils.savePattern(mRobotium);
		
		//Clicking on two of the three texts
		TUtils.clickOnListItemText(mRobotium, feeling_item_prefix + 0);
		TUtils.clickOnListItemText(mRobotium, feeling_item_prefix + 2);
		
		//Swipe to the right
		TUtils.scrollSideways(mRobotium, LeftOrRight.RIGHT);
		assertTrue(mRobotium.waitForView(R.id.list_item_activeCheckBox));
		
		//Calculate sort value and compare
		int tmpNumberOfItemsChecked = 2;
		int tmpNumberOfItemsInPattern = 4;
		int tmpNumberOfMatches = 2;
		float tmpRelevance = (float)tmpNumberOfMatches / (float)(tmpNumberOfItemsInPattern + tmpNumberOfItemsChecked);
		double tmpResult = tmpRelevance * SortingAlgorithmServiceM.PATTERN_MULTIPLIER
				+ SortingAlgorithmServiceM.SIMPLE_PATTERN_MATCH_ADDITION;
		
		
		
		
		
		
		//TODO
		/*
		findTopmostListItemInDatabaseAndCompare
		
		assertTrue(mRobotium.searchText(Utils.formatNumber(tmpResult))); //-javadoc: "automatically scrolls when needed"
		*/
	}
	
	//Integer division is used in many places
	public void largeTestAlgorithmPatternLimit(){
		final String feeling_item_prefix = "Feeling item nr ";
		///final String need_item_prefix = "Need item nr ";
		final int number_of_items_to_check = 7;
		final int number_of_rows_over_max = 20;
		
		ArrayList<Integer> tmpRowsInPatterns = new ArrayList<Integer>();
		int tmpCalculatedTestValue = 0;
		
		//Create three new Feeling items
		TUtils.addListItems(mRobotium, number_of_items_to_check, BackNavType.TEMPORAL, feeling_item_prefix);
		
		for(int i = 0; i < OtherU.MAX_NR_OF_PATTERN_ROWS + number_of_rows_over_max;
				i = i + number_of_items_to_check){
			//Click the new feeling items
			for(int j = 0; j < number_of_items_to_check; j++){
				TUtils.clickOnListItemText(mRobotium, feeling_item_prefix + j);
			}
			
			//Saving the new pattern and updating test values
			TUtils.savePattern(mRobotium); //-any limiting of the number of rows will take place here
			tmpCalculatedTestValue = tmpCalculatedTestValue + number_of_items_to_check;
			tmpRowsInPatterns.add(Integer.valueOf(number_of_items_to_check));
			
			//Checking that the number of rows in the patterns table is as many as expected
			Cursor tmpPatternsCursor = getActivity().getContentResolver().query(
					ContentProviderM.PATTERNS_CONTENT_URI, null, null, null, null);
			
			//Limiting the test value
			while(true){
				if(tmpCalculatedTestValue <= OtherU.MAX_NR_OF_PATTERN_ROWS){
					break;
				}else{
					tmpCalculatedTestValue = tmpCalculatedTestValue - tmpRowsInPatterns.get(0);
					tmpRowsInPatterns.remove(0);
				}
			}
			
			String tmpMessage = "assertTrue in testAlgorithmPatternLimit failed when adding row number " + i + ". ";
			tmpMessage = tmpMessage + "tmpPatternsCursor.getCount() = " + tmpPatternsCursor.getCount() + ". ";
			tmpMessage = tmpMessage + "tmpCalculatedTestValue = " + tmpCalculatedTestValue + ". ";
			assertTrue(tmpMessage, tmpPatternsCursor.getCount() == tmpCalculatedTestValue);
		}
	}
	
	public void testRandomActions(){
		
		int tmpSeed = 42;
		
		RandomActions tmpRandomActions = new RandomActions();
		
		tmpRandomActions.performRandomActions(getActivity(), getInstrumentation(), mRobotium, 25, tmpSeed);
		
	}
	
	
	public void largeTestInfiniteRandomActions(){
		
		int tmpSeed = 24;
		
		RandomActions tmpRandomActions = new RandomActions();
		
		tmpRandomActions.performRandomActions(getActivity(), getInstrumentation(), mRobotium,
				Integer.MAX_VALUE, tmpSeed);
		
	}

	public void testFinishActivity(){
		mRobotium.finishOpenedActivities();
	}

	//TODO: Memory test. How??
	/*
	Please note that we cannot test killing the process because the instrumentation will fail
	android.os.Process.killProcess(android.os.Process.myPid());
	
	 * "You can just use the shell to kill your app's process. All the system does when low on memory is
	 *  select processes to kill to reclaim more, so your code will be going through pretty much the same
	 *  experience if you manually kill it yourself."
	 * Dianne Hackborn, Android framework engineer
	 * https://groups.google.com/forum/#!topic/android-platform/LbYC8O7Cv7k
	 */
}

	/*
	public void testAlgorithmSimpleClick(){
		//Click on one button and save
		mRobotium.clickOnCheckBox(0);
		mRobotium.clickOnActionBarItem(R.id.menu_item_save_pattern);
		
		//Clear the result from the program and restart..
		getActivity().fireResetDataEvent();
		TUtils.clearAllFiles(getActivity());
		TUtils.restartMainActivity(getActivity());
		//-Loading the result from a file is done automatically when starting anew
		//..verify that the checkbox is _un_checked after the restart
		assertFalse(mRobotium.isCheckBoxChecked(0));
		
		View tmpTextView = null;
		boolean tmpHasFoundCheckBoxView = false;
		int i = 0;
		do{
			tmpTextView = mRobotium.getView(R.id.list_item_titleTextView, i);
			if(((TextView)tmpTextView).getText().toString().contains("Thinking")){
				CheckBox tmpCheckBoxView = (CheckBox)mRobotium.getView(R.id.list_item_activeCheckBox, i);
				assertFalse(tmpCheckBoxView.isChecked());
				assertTrue(((TextView)tmpTextView).getText().toString().contains("1.0"));
				tmpHasFoundCheckBoxView = true;
				break;
			}
			i++;
		}while(tmpTextView != null);
		if(!tmpHasFoundCheckBoxView){
			assertTrue(false);
		}
		
		assertFalse(mRobotium.searchText("2.0", 1, true));
	}
	*/
	
	
	
	/*
	//Verifying Feelings list
	tmpTextView = null;
	i = 0;
	//for(int j = 0; j < ){
	do{
		//try{}catch(Exception)
		tmpTextView = mRobotium.getView(R.id.list_item_titleTextView, i);
		CheckBox tmpCheckBoxView = (CheckBox)mRobotium.getView(R.id.list_item_activeCheckBox, i);
		if(((TextView)tmpTextView).getText().toString().contains(new_need_item_title_1)){
			Log.d(Utils.getClassName(),"Expected value = " + tmpExpectedValueAsString
					+ " | Received value = " + ((TextView)tmpTextView).getText().toString());
			assertTrue(((TextView)tmpTextView).getText().toString().contains(tmpExpectedValueAsString));
			break;
		}
		//assertFalse(tmpCheckBoxView.isChecked());
		i++;
	}while(tmpTextView != null);
	*/

