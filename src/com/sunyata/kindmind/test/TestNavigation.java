package com.sunyata.kindmind.test;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.LinearLayout;

import com.robotium.solo.Solo;
import com.sunyata.kindmind.R;
import com.sunyata.kindmind.List.ListTypeM;
import com.sunyata.kindmind.Setup.BookmarkChooserActivityC;
import com.sunyata.kindmind.Setup.ItemSetupActivityC;
import com.sunyata.kindmind.main.MainActivityC;
import com.sunyata.kindmind.test.TUtils.ActivityType;
import com.sunyata.kindmind.test.TUtils.BackNavType;
import com.sunyata.kindmind.test.TUtils.ClickType;
import com.sunyata.kindmind.test.TUtils.Coverage;
import com.sunyata.kindmind.test.TUtils.LeftOrRight;
//-importing the R file from the project under test

public class TestNavigation extends ActivityInstrumentationTestCase2<MainActivityC> {
	private Solo mRobotium;
	public TestNavigation() {
		super(MainActivityC.class);
	}
	protected void setUp() throws Exception { //Is run before each test
		super.setUp();
		mRobotium = TUtils.setup(getInstrumentation(), getActivity());
	}
	protected void tearDown() throws Exception { //Is run after each test
		//super.tearDown(); //This line has been removed in the Robotium example
		TUtils.tearDown(getInstrumentation(), mRobotium, getActivity());
	}
	
	/*
	 * Improvements: waitForFragmentByTag would be nice to use to verify, see this link for more info:
	 *  http://stackoverflow.com/questions/6374170/how-to-set-a-fragment-tag-by-code#6382672
	 */
	@SmallTest
	public void testMainActivityClickTabs(){
		mRobotium.clickOnText(mRobotium.getString(R.string.feelings_title));
		TUtils.verifyViewPagerPos(mRobotium, getActivity(), ListTypeM.FEELINGS);
		
		mRobotium.clickOnText(mRobotium.getString(R.string.needs_title));
		TUtils.verifyViewPagerPos(mRobotium, getActivity(), ListTypeM.NEEDS);

		mRobotium.clickOnText(mRobotium.getString(R.string.kindness_title));
		TUtils.verifyViewPagerPos(mRobotium, getActivity(), ListTypeM.KINDNESS);
	}
	
	@SmallTest
	public void testMainActivitySwipe(){
		TUtils.verifyViewPagerPos(mRobotium, getActivity(), ListTypeM.FEELINGS);
		
		TUtils.scrollSideways(mRobotium, LeftOrRight.RIGHT);
		TUtils.verifyViewPagerPos(mRobotium, getActivity(), ListTypeM.NEEDS);
		
		TUtils.scrollSideways(mRobotium, LeftOrRight.RIGHT);
		TUtils.verifyViewPagerPos(mRobotium, getActivity(), ListTypeM.KINDNESS);
		
		TUtils.scrollSideways(mRobotium, LeftOrRight.LEFT);
		TUtils.verifyViewPagerPos(mRobotium, getActivity(), ListTypeM.NEEDS);
	}
	
	@SmallTest
	public void testMainActivityClearAll(){
		TUtils.verifyViewPagerPos(mRobotium, getActivity(), ListTypeM.FEELINGS);
		
		TUtils.scrollSideways(mRobotium, LeftOrRight.RIGHT);
		TUtils.verifyViewPagerPos(mRobotium, getActivity(), ListTypeM.NEEDS);
		
		TUtils.scrollSideways(mRobotium, LeftOrRight.RIGHT);
		TUtils.verifyViewPagerPos(mRobotium, getActivity(), ListTypeM.KINDNESS);
		
		TUtils.clearChecks(mRobotium);
	}
	
	@SmallTest
	public void testMainActivitySave(){
		TUtils.verifyViewPagerPos(mRobotium, getActivity(), ListTypeM.FEELINGS);
		
		TUtils.scrollSideways(mRobotium, LeftOrRight.RIGHT);
		TUtils.verifyViewPagerPos(mRobotium, getActivity(), ListTypeM.NEEDS);
		
		TUtils.scrollSideways(mRobotium, LeftOrRight.RIGHT);
		TUtils.verifyViewPagerPos(mRobotium, getActivity(), ListTypeM.KINDNESS);
		
		TUtils.savePattern(mRobotium);
	}
	
	//waitForText, waitForActivity
	@SmallTest
	public void testAbout(){
		TUtils.navigateToAbout(mRobotium, BackNavType.TEMPORAL);
		TUtils.navigateToAbout(mRobotium, BackNavType.ANCESTRAL);
	}
	
	@SmallTest
	public void testItemSetupNewItem(){
		TUtils.addListItems(mRobotium, 1, BackNavType.TEMPORAL, "New item nr ");
		TUtils.addListItems(mRobotium, 1, BackNavType.ANCESTRAL, "New item nr ");
	}
	
	@SmallTest
	public void testItemSetupLongPress(){
		//Feelings
		TUtils.clickListItems(
				getActivity(), getInstrumentation(), mRobotium, Coverage.INTELLIGENT, true, ClickType.LONG);
		
		//Needs
		mRobotium.clickOnText(mRobotium.getString(R.string.needs_title));
		TUtils.clickListItems(
				getActivity(), getInstrumentation(), mRobotium, Coverage.INTELLIGENT, true, ClickType.LONG);
		
		//Kindness
		mRobotium.clickOnText(mRobotium.getString(R.string.kindness_title));
		TUtils.clickListItems(
				getActivity(), getInstrumentation(), mRobotium, Coverage.INTELLIGENT, true, ClickType.LONG);
	}
	
	@SmallTest
	public void testBookmarkChooser(){
		mRobotium.clickOnText(mRobotium.getString(R.string.kindness_title));
		TUtils.clickListItems(
				getActivity(), getInstrumentation(), mRobotium, Coverage.FIRST, false, ClickType.LONG);
		
		final int finalNumberOfActionsInListBeforeAdding =
				((LinearLayout)mRobotium.getView(R.id.actionsLinearLayout)).getChildCount();
		
		mRobotium.clickOnText(mRobotium.getString(R.string.itemSetup_addNewAction_title));
		assertTrue(mRobotium.waitForDialogToOpen());
		
		mRobotium.clickOnText(mRobotium.getString(R.string.itemSetup_bookmarkChooser));
		assertTrue(mRobotium.waitForActivity(BookmarkChooserActivityC.class));
		
		//Choosing one of the bookmarks
		TUtils.clickInMiddleOfScreen(mRobotium, ClickType.STANDARD, ActivityType.DETAILS_CHOOSER);
		assertTrue(mRobotium.waitForActivity(ItemSetupActivityC.class));
		
		assertTrue(((LinearLayout)mRobotium.getView(R.id.actionsLinearLayout)).getChildCount()
				== finalNumberOfActionsInListBeforeAdding + 1);
	}
	
	@SmallTest
	public void testScrollInLists(){
		//Alternatives: scrollDown, scrollDownList (2 versions), scrollListToLine

		//Feelings list
		TUtils.scrollSideways(mRobotium, LeftOrRight.RIGHT);
		while(mRobotium.scrollDown()){}
		while(mRobotium.scrollUp()){}

		//Needs list
		TUtils.scrollSideways(mRobotium, LeftOrRight.RIGHT);
		while(mRobotium.scrollDown()){}
		while(mRobotium.scrollUp()){}
		
		//Kindness list
		TUtils.scrollSideways(mRobotium, LeftOrRight.RIGHT);
		while(mRobotium.scrollDown()){}
		while(mRobotium.scrollUp()){}
		
		//Checking that the position is kept
		TUtils.scrollSideways(mRobotium, LeftOrRight.LEFT); //-needs
		TUtils.scrollSideways(mRobotium, LeftOrRight.LEFT); //-feelings
		while(mRobotium.scrollDown()){}
		TUtils.scrollSideways(mRobotium, LeftOrRight.RIGHT); //-needs
		while(mRobotium.scrollDown()){}
		while(mRobotium.scrollUp()){}
		TUtils.scrollSideways(mRobotium, LeftOrRight.RIGHT); //-kindness
		assertFalse(mRobotium.scrollUp());
		TUtils.scrollSideways(mRobotium, LeftOrRight.LEFT); //-needs
		assertFalse(mRobotium.scrollUp());
		TUtils.scrollSideways(mRobotium, LeftOrRight.LEFT); //-feelings
		assertFalse(mRobotium.scrollDown()); //-this always fails, even though we can see that we are at the bottom 
	}
}