package com.sunyata.kindmind.test;

import java.util.ArrayList;
import java.util.Random;

import android.app.Instrumentation;
import android.util.Log;

import com.robotium.solo.Solo;
import com.sunyata.kindmind.main.MainActivityC;
import com.sunyata.kindmind.test.TUtils.BackNavType;
import com.sunyata.kindmind.test.TUtils.ClickType;
import com.sunyata.kindmind.test.TUtils.Coverage;
import com.sunyata.kindmind.util.DbgU;

public class RandomActions {

	private MainActivityC mActivity;
	private Instrumentation mInstr;
	private Solo mRobotium;
	private int mSeed;
	
	public void performRandomActions(MainActivityC inActivity, Instrumentation inInstr,
			Solo inRobotium, int inNumberOfActions, int inSeed){
		
		mActivity = inActivity;
		mInstr = inInstr;
		mRobotium = inRobotium;
		mSeed = inSeed;
		
		Random tmpPseudoRandom = new Random(mSeed);
		
		ArrayList<TestFunction> tmpListOfTestFunctions = new ArrayList<TestFunction>();
		tmpListOfTestFunctions.add(new AddListItemFunction());
		tmpListOfTestFunctions.add(new LongPressListItemsFunction());
		tmpListOfTestFunctions.add(new ClickListItemsFunction());
		tmpListOfTestFunctions.add(new SwitchOrientationBackAndForthFunction());
		tmpListOfTestFunctions.add(new SwitchOrientationOneWayFunction());
		tmpListOfTestFunctions.add(new AboutFunction());
		//-if we want one function to appear more often we can simply add it more times
		int tmpActionTypeIndexRandom;
		
		for(int i = 0; i < inNumberOfActions; i++){
			Log.i(DbgU.getAppTag(), DbgU.getMethodName() + "Starting random action number " + i);
			tmpActionTypeIndexRandom = tmpPseudoRandom.nextInt(tmpListOfTestFunctions.size());
			tmpListOfTestFunctions.get(tmpActionTypeIndexRandom).doFunction();
		}
	}
	
	//Important that we end up where we started
	public abstract class TestFunction{
		public TestFunction(){
		}
		public void doFunction(){ //TODO: adding backnav
			Log.i(DbgU.getAppTag(), "Random function " + DbgU.getMethodName() + ", mSeed = " + mSeed);
		};
	}
	public class AddListItemFunction extends TestFunction{
		@Override
		public void doFunction() {
			super.doFunction();
			TUtils.addListItems(mRobotium, 3, BackNavType.TEMPORAL, "AddListItem ");
		}
	}
	/*
	public class DeleteListItemFunction extends TestFunction{
		@Override
		public void doFunction() {
			super.doFunction();
			TUtils.delete    .addListItems(mRobotium, 3, BackNavType.TEMPORAL, "AddListItem ");
		}
	}
	*/
	public class LongPressListItemsFunction extends TestFunction{
		@Override
		public void doFunction() {
			super.doFunction();
			TUtils.clickListItems(mActivity, mInstr, mRobotium, Coverage.INTELLIGENT, true, ClickType.LONG);
		}
	}
	
	public class ClickListItemsFunction extends TestFunction{
		@Override
		public void doFunction() {
			super.doFunction();
			TUtils.clickListItems(mActivity, mInstr, mRobotium, Coverage.INTELLIGENT, true, ClickType.STANDARD);
		}
	}
	
	public class SwitchOrientationBackAndForthFunction extends TestFunction{
		@Override
		public void doFunction() {
			super.doFunction();
			mRobotium.setActivityOrientation(Solo.LANDSCAPE);
			mRobotium.setActivityOrientation(Solo.PORTRAIT);
		}
	}
	
	public class SwitchOrientationOneWayFunction extends TestFunction{
		@Override
		public void doFunction() {
			super.doFunction();
			TUtils.switchOrientation(mRobotium, mActivity);
		}
	}
	
	public class AboutFunction extends TestFunction{
		@Override
		public void doFunction() {
			super.doFunction();
			TUtils.navigateToAbout(mRobotium, BackNavType.TEMPORAL);
			TUtils.navigateToAbout(mRobotium, BackNavType.ANCESTRAL);
		}
	}
	
	/*
	public class AboutFunction extends TestFunction{
		public AboutFunction() {
			super();
		}
		@Override
		public void doFunction() {
			super.doFunction();
			TUtils.longPressListItems(mActivity, mInstr, mRobotium, TestCoverage.INTELLIGENT, true);
		}
	}
	*/
}
