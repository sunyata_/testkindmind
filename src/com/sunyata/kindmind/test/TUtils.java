package com.sunyata.kindmind.test;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.app.Instrumentation;
import android.graphics.Point;
import android.util.Log;

import com.robotium.solo.Condition;
import com.robotium.solo.Solo;
import com.sunyata.kindmind.AboutActivityC;
import com.sunyata.kindmind.R;
import com.sunyata.kindmind.Setup.ItemSetupActivityC;
import com.sunyata.kindmind.main.MainActivityC;
import com.sunyata.kindmind.util.DbgU;

public class TUtils {

	//-------------------Enums and constants
	
	enum LeftOrRight{LEFT,RIGHT;}
	enum BackNavType{ANCESTRAL,TEMPORAL};
	enum Coverage{FIRST,FULL,INTELLIGENT,RANDOM};
	enum ClickType{STANDARD,LONG};
	enum ActivityType{MAIN_ACTIVITY, DETAILS_CHOOSER};
	
	private static final int WAIT_TIME_FOR_SMOOTH_SCROLL = 1500;
	private static final int WAIT_TIME_SHORT = 750;
	
	private static final String APP_TAG = "testkindmind";
	
	//-------------------Setup and teardown methods
	
	public static Solo setup(Instrumentation inInstrumentation, MainActivityC inMainActivityC) {
		//Clearing data
		inMainActivityC.fireResetDataEvent();
		TUtils.clearAllFiles(inMainActivityC);
		
		Solo retRobotium = new Solo(inInstrumentation, inMainActivityC);
		retRobotium.assertMemoryNotLow(); //-not necassary maybe, but has never failed
		return retRobotium;
	}
	public static void tearDown(Instrumentation inInstr, Solo inRobotium, final MainActivityC inMainActivityC)
			throws Exception { //-Is run after each test
		/*
		//Clearing data
		inInstr.runOnMainSync(new Runnable(){
			@Override
			public void run() {
				inMainActivityC.fireResetDataEvent();
			}
		});
		inInstr.waitForIdleSync();
		*/
		TUtils.clearAllFiles(inMainActivityC);

		inRobotium.assertMemoryNotLow(); //-not necassary maybe, but has never failed
		inRobotium.finishOpenedActivities();
	}
	
	
	//-------------------Clearing, restarting
	
	static void clearAllFiles(Activity inActivity){
		File[] tmpFileListArray = inActivity.getFilesDir().listFiles(); //-Please note: activity under test
		for(int i = 0; i < tmpFileListArray.length; i++){
			File f = tmpFileListArray[i];
			boolean tmpDeleteSuccessful = f.delete();
			junit.framework.Assert.assertTrue(tmpDeleteSuccessful);
		}
	}

	
	//-------------------ViewPager (MainActivityC) and list (ListFragmentC) navigation
	
	static void scrollSideways(Solo inRobotium, LeftOrRight inDirection) {
		Point outSize = new Point();
		inRobotium.getCurrentActivity().getWindowManager().getDefaultDisplay().getSize(outSize);

		final int MARGINAL = 10;
		
		switch(inDirection){
		case LEFT: inRobotium.drag(MARGINAL, outSize.x-MARGINAL, outSize.y/2, outSize.y/2, 1); break;
		case RIGHT: inRobotium.drag(outSize.x-MARGINAL, MARGINAL, outSize.y/2, outSize.y/2, 1); break;
		default: Log.wtf(DbgU.getAppTag(), DbgU.getMethodName() + " Case not covered");
		}
	}
	

	
	private static void navigateBack(Solo inRobotium, BackNavType inBackNavType){
		switch(inBackNavType){
		case ANCESTRAL: inRobotium.clickOnActionBarHomeButton(); break;
		case TEMPORAL: inRobotium.goBack(); break;
		default: Log.wtf(DbgU.getAppTag(), DbgU.getMethodName() + " Case not covered");
		}
	}
		
	public static void verifyViewPagerPos(Solo inRobotium, final MainActivityC inActivity, final int inListType) {
		inRobotium.waitForCondition(new Condition(){
			@Override
			public boolean isSatisfied() {
				return inActivity.getCurrentAdapterPosition() == inListType;
			}
		}, (new Solo.Config()).timeout_large);
	}
	
	
	//-------------------List Items
	
	public static void addListItems(Solo inRobotium, int inNumberOfItems, BackNavType inBackNavType, String inPrefix) {
		junit.framework.Assert.assertTrue(inRobotium.waitForActivity(MainActivityC.class));
		for(String ldiName : generateItemNames(inNumberOfItems, inPrefix)){
			inRobotium.clickOnActionBarItem(R.id.menu_item_new_listitem);
			junit.framework.Assert.assertTrue(inRobotium.waitForActivity(ItemSetupActivityC.class));
			inRobotium.enterText(0, ldiName);
			navigateBack(inRobotium, inBackNavType);
			junit.framework.Assert.assertTrue(inRobotium.waitForActivity(MainActivityC.class));
		}
	}
	private static String[] generateItemNames(int inNumberOfNames, String inPrefix){
		String[] retNamesArray = new String[inNumberOfNames]; //new ArrayList<String>();
		for(int i = 0; i < inNumberOfNames; i++){
			retNamesArray[i] = inPrefix + i;
		}
		return retNamesArray;
	}

	public static void clickListItems(final MainActivityC inActivity, Instrumentation inInstr,
			Solo inRobotium, Coverage inTestCoverage, boolean inNavBack, ClickType inClickType) {
		
		junit.framework.Assert.assertTrue(inRobotium.waitForView(android.R.id.list)); //@id/android:list
		
		
		/*
		scrolllisttotop(0)
		
		inRobotium.clickInList(line)
		
		scrollListToLine(index,)
		
		scrolllisttobottom(0)
		
		clicklonginlist
		*/

		inRobotium.scrollListToTop(0);
		
		for(int i = 0; inRobotium.scrollDown(); i++){
			switch(inTestCoverage){
			case FIRST: if(i == 0){break;}else{continue;}
			case FULL: break;
			case INTELLIGENT: if(i == 0 || i == 1){break;}else{continue;}
			default: Log.wtf(DbgU.getAppTag(), DbgU.getMethodName() + " Case not covered");
			}
			
			switch(inClickType){
			case LONG:
				clickInMiddleOfScreen(inRobotium, ClickType.LONG, ActivityType.MAIN_ACTIVITY);
				junit.framework.Assert.assertTrue(inRobotium.waitForActivity(ItemSetupActivityC.class));
				if(inNavBack){
					navigateBack(inRobotium, BackNavType.TEMPORAL);
					//-bug in Robotium: if ancestral, we get scrolling in the list and freezing (strange, unknown why,
					// we know that we have the right view in tmpListItemView)
					junit.framework.Assert.assertTrue(inRobotium.waitForActivity(MainActivityC.class));
				}
				break;
			case STANDARD:
				clickInMiddleOfScreen(inRobotium, ClickType.STANDARD, ActivityType.MAIN_ACTIVITY);
				waitForAlgorithmAndScrollUp(inRobotium);
			}
		}
	}

	public static void clickOnListItemText(Solo inRobotium, String inTextToClick) {
		inRobotium.clickOnText(inTextToClick, 0, true);
		waitForAlgorithmAndScrollUp(inRobotium);
	}
	
	
	//-------------------Other
	
	public static void savePattern(Solo inRobotium) {
		inRobotium.clickOnActionBarItem(R.id.menu_item_save_pattern);
		waitForAlgorithmAndScrollUp(inRobotium);
	}
	public static void clearChecks(Solo inRobotium) {
		inRobotium.clickOnActionBarItem(R.id.menu_item_clear_all_list_selections);
		waitForAlgorithmAndScrollUp(inRobotium);
	}
	
	public static void clickInMiddleOfScreen(Solo inRobotium, ClickType inClickType, ActivityType inActivityType) {
		inRobotium.sleep(WAIT_TIME_SHORT);
		Point outSize = new Point();
		inRobotium.getCurrentActivity().getWindowManager().getDefaultDisplay().getSize(outSize);
		switch(inClickType){
		case LONG:		inRobotium.clickLongOnScreen(outSize.x/2, outSize.y/2); break;
		case STANDARD:	inRobotium.clickOnScreen(outSize.x/2, outSize.y/2, 1); break; //-"1" is the number of clicks
		default: Log.wtf(DbgU.getAppTag(), DbgU.getMethodName() + " Case not covered");
		}
		switch(inActivityType){
		case MAIN_ACTIVITY:		inRobotium.waitForActivity(MainActivityC.class); break;
		case DETAILS_CHOOSER:	inRobotium.sleep(WAIT_TIME_SHORT); break;
		default: Log.wtf(DbgU.getAppTag(), DbgU.getMethodName() + " Case not covered");
		}
	}

	private static void waitForAlgorithmAndScrollUp(Solo inRobotium) {
		junit.framework.Assert.assertTrue(inRobotium.waitForView(android.R.id.list));
		inRobotium.sleep(WAIT_TIME_FOR_SMOOTH_SCROLL);
		junit.framework.Assert.assertFalse(inRobotium.scrollUp());
	}

	public static void execCommandInShell(String[] inCommandString) {
		try {
			Runtime.getRuntime().exec(inCommandString);
		} catch (IOException e) {
			Log.wtf(DbgU.getAppTag(), DbgU.getMethodName() + " Case not covered");
			e.printStackTrace();
		}
	}
	public static void navigateToAbout(Solo inRobotium, BackNavType inBackNavType) {
		junit.framework.Assert.assertTrue(inRobotium.waitForActivity(MainActivityC.class));
		inRobotium.clickOnActionBarItem(R.id.menu_item_about);
		junit.framework.Assert.assertTrue(inRobotium.waitForActivity(AboutActivityC.class));
		navigateBack(inRobotium, inBackNavType);
		junit.framework.Assert.assertTrue(inRobotium.waitForActivity(MainActivityC.class));
	}
	
	public static void switchOrientation(Solo inRobotium, Activity inActivity){
		int tmpOrientation = inActivity.getResources().getConfiguration().orientation;
		switch(tmpOrientation){
		case Solo.PORTRAIT: tmpOrientation = Solo.LANDSCAPE; break;
		case Solo.LANDSCAPE: tmpOrientation = Solo.PORTRAIT; break;
		default: Log.wtf(DbgU.getAppTag(), DbgU.getMethodName() + " Case not covered");
		}
		
		inRobotium.setActivityOrientation(tmpOrientation);
		junit.framework.Assert.assertTrue(inRobotium.waitForActivity(MainActivityC.class));
	}
	public static String getAppTag() {
		// TODO Auto-generated method stub
		return APP_TAG;
	}
	
	//Idea: findListItemByText
	
	//-------------------Unused, but storing these for the future
	/*
	static void restartMainActivity(Activity inActivity, Solo inRobotium){
		inRobotium.finishOpenedActivities();
		
		Context tmpBaseContext = inActivity.getBaseContext();
		Intent intent = tmpBaseContext.getPackageManager().getLaunchIntentForPackage(tmpBaseContext.getPackageName());
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
		//-FLAG_ACTIVITY_CLEAR_TASK could maybe be used and we would not have to call finishopenedactivities?
		inActivity.startActivity(intent);
	}
	*/
	/*
	static void clearAppDataUnused() {
		String[] tmpCmd = new String[]{"adb","shell","pm","clear","com.sunyata.kindmind"};
		try {
			Runtime.getRuntime().exec(tmpCmd);
		} catch (IOException e) {
			Log.e(Utils.getClassName(), "Error in ClearAppData: exec method");
			e.printStackTrace();
		}
	}
	*/
	/*
	private static String[] listOfStringsFromListViewUnused(ListView inListView){
		String[] retListViewStringArray = new String[inListView.getCount()];
		View tmpListItemView = null;
		TextView tmpTextView = null;
		for(int i = 0; i < inListView.getCount(); i++){
			tmpListItemView = inListView.getAdapter().getView(i, null, inListView);
			tmpTextView = (TextView)tmpListItemView.findViewById(R.id.list_item_titleTextView);
			retListViewStringArray[i] = tmpTextView.getText().toString();
		}
		return retListViewStringArray;
	}
	 */
	
	/*
	public static void clickListItems_DirectAccess(final MainActivityC inActivity, Instrumentation inInstr,
			Solo inRobotium, Coverage inTestCoverage, boolean inNavBack, ClickType inClickType) {
		
		junit.framework.Assert.assertTrue(inRobotium.waitForView(android.R.id.list)); //@id/android:list
		
		

		
		junit.framework.Assert.assertTrue(inRobotium.waitForCondition(new Condition(){
			@Override
			public boolean isSatisfied() {
				return inActivity.isListViewPresent();
			}
		}, new Solo.Config().timeout_large));
		
		ListView refListView = inActivity.getListViewOfCurrentFragment();
		//-Please note that "(ListView)inRobotium.getView(android.R.id.list, 0)" does not work
		int tmpNumberOfListItems = refListView.getCount();
		
		for(int i = 0; i < tmpNumberOfListItems; i++){
			switch(inTestCoverage){
			case FIRST: if(i == 0){break;}else{continue;}
			case FULL: break;
			case INTELLIGENT:
				if(i == 0 || i == 1 || i == tmpNumberOfListItems - 2 || i == tmpNumberOfListItems - 1){
					break;
				}else{
					continue;
				}
			default: Log.e(Utils.getClassName(), "Error in method longPressListItems: Case not covered");
			}

			scrollListToPosition(inInstr, refListView, i);

			int tmpVisibleIndex = i - refListView.getFirstVisiblePosition();
			View tmpListItemView = refListView.getChildAt(tmpVisibleIndex);
			inRobotium.clickLongOnView(tmpListItemView);
			
			junit.framework.Assert.assertTrue(inRobotium.waitForActivity(ItemSetupActivityC.class));
			
			if(inNavBack){
				navigateBack(inRobotium, BackNavType.TEMPORAL);
				//-bug in Robotium: if ancestral, we get scrolling in the list and freezing (strange, unknown why,
				// we know that we have the right view in tmpListItemView)
				junit.framework.Assert.assertTrue(inRobotium.waitForActivity(MainActivityC.class));
			}
		}
	}
	private static void scrollListToPosition_DirectAccess(Instrumentation inInstr, final ListView inListView, final int inPos){
		inInstr.runOnMainSync(new Runnable(){
			@Override
			public void run() {
				inListView.setSelection(inPos);
			}
		});
		inInstr.waitForIdleSync();
	}
	*/
}
